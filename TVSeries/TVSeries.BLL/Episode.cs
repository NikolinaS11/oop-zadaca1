﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TVSeries.BLL
{

    public class Episode
    {
        private static int Counter = 0;
        public int Id { get; private set; }
        public int ViewCount { get; private set; }        public double RatingSum { get; private set; }        public List<double> Ratings { get; private set; }
        public double MaxScore { get; private set; }
        public Episode()
        {
            Ratings = new List<double>();
            Id = Counter;
            Counter++;
        }

        public Episode(int viewCount, double ratingSum, double maxScore)
        {
            ViewCount = viewCount;
            RatingSum = ratingSum;
            MaxScore = maxScore;
            Ratings = new List<double>();
            Id = Counter;
            Counter++;
        }

        public double GetMaxScore()
        {
            return Ratings.Max();
        }

        public void AddView(double score)
        {
            ViewCount++;
            Ratings.Add(score);
        }

        public double GetAverageScore()
        {
            if (Ratings.Any())
            {
                return Ratings.Average();
            }
            else
            {
                return 0;
            }
        }

        public int GetViewerCount()
        {
            return ViewCount;
        }

    }
}
